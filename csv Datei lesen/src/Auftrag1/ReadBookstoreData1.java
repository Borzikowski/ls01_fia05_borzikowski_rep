package Auftrag1;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class ReadBookstoreData1 {

  public static void main(String[] args) {
	  
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    try {
         DocumentBuilder dbuilder = factory.newDocumentBuilder();
         Document doc = dbuilder.parse("src/Auftrag1/buchhandlung1.xml");
         
    	Node titel = doc.getElementsByTagName("titel").item(0);
    	Element titelElem = (Element) titel;
    	System.out.println(titelElem.getNodeName() + ": " + titelElem.getTextContent());
    } catch (ParserConfigurationException e) {
        e.printStackTrace();
        
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();	
    }
    } 

  }


